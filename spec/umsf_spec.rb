require "spec_helper"

RSpec.describe Umsf do
  it 'create file with plain text' do
    expect(File.file?(fixture_path('exampleasdf.html'))).to be false
    expect(File.file?(fixture_path('example2.html'))).to be false

    Umsf.visit(fixture_root_path)

    expect(File.file?(fixture_path('exampleasdf.html'))).to be true
    expect(File.file?(fixture_path('example2.html'))).to be true
  end

  # [
  #   ['/home/bjorn/projects/umsf/spec/fixtures/exa mple.asdf.doc',
  #    '/home/bjorn/projects/umsf/spec/fixtures/exampleasdf.doc'],
  #   ['/Колектив кафедри(доповнити основні наукові праці)/Барабаш Ю.В..docx',
  #    '/Колектив кафедри(доповнити основні наукові праці)/БарабашЮВ.docx'],
  #   ['/Колектив кафедри(доповнити основні наукові праці)/Барабаш Ю.В..txt',
  #    '/Колектив кафедри(доповнити основні наукові праці)/Барабаш Ю.В..txt']
  # ].each do |entry|
  #   path, expectation = entry
  #   it 'change path name' do
  #     expect(Umsf::Visitor.new.sanitize_filename(path)).to eq expectation
  #   end
  # end
end
