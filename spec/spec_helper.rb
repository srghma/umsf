require 'bundler/setup'
require 'umsf'

module FixturesHelper
  FIXTURES_ROOT = File.expand_path('../fixtures', __FILE__)

  module_function

  def fixture_root_path
    FIXTURES_ROOT
  end

  def fixture_path(name)
    "#{FIXTURES_ROOT}/#{name}"
  end
end

RSpec.configure do |config|
  config.example_status_persistence_file_path = '.rspec_status'

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.include FixturesHelper

  config.before(:suite) do
    Dir.chdir(FixturesHelper.fixture_root_path) do
      FileUtils.rm Dir.glob('*.txt')
      FileUtils.rm Dir.glob('*.html')
      FileUtils.rm Dir.glob('*.jpg')
    end
  end
end
