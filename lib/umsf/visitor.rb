module Umsf
  class Visitor
    def initialize(path)
      path = File.expand_path path
      return visit_dir(path) if File.directory?(path)

      dir = File.dirname path
      Dir.chdir(dir) do
        visit_file(path)
      end
    end

    def visit_dir(path)
      Dir.chdir(path) do
        Dir.glob('*').each do |entry|
          entry = "#{path}/#{entry}"
          File.directory?(entry) ? visit_dir(entry) : visit_file(entry)
        end
      end
    end

    def visit_file(path)
      return unless path =~ /.doc[x]?$/i
      newpath = sanitize_filename(path)
      if newpath != path
        File.rename(path, newpath)
        path = newpath
      end

      Parser.new(path).parse_and_save
      Extractor.new(path).extract_images
    end

    def sanitize_filename(oldpath)
      return oldpath unless oldpath =~ /.doc[x]?$/i
      extname  = File.extname(oldpath)
      basename = File.basename(oldpath, '.*')
      basename.delete!('. ')
      File.dirname(oldpath) + '/' + basename + extname
    end
  end
end
