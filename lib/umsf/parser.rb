module Umsf
  class Parser
    def initialize(path)
      @path = path
    end

    def parse_and_save
      convert_to_text

      @content = File.readlines(save_as)
      @content.map(&:strip!)
      @content.reject!(&:empty?)
      remove_numbers
      surround_with_p
      prettify_scholar_link
      h3_what_you_can

      File.write(save_as, @content.join("\n"))
    end

    def convert_to_text
      require 'shellwords'
      path = Shellwords.escape(@path)
      save = Shellwords.escape(save_as)
      `abiword --to=txt #{path} --to-name #{save}`
    end

    private

    def remove_numbers
      @content.map { |line| line.gsub!(/^\s*\d{1,2}\.\s*/, '') }
    end

    def surround_with_p
      @content.map { |line| line.insert(0, '<p>').insert(-1, '</p>') }
    end

    def prettify_scholar_link
      @content.map do |line|
        line.gsub!(
          %r{<p>.*Scholar.*</p>},
          '<a href="#">Профіль у Google Scholar</a>'
        )
      end
    end

    def h3_what_you_can
      @content.map do |line|
        line.gsub!(
          h3_regex,
          '<h3>\1</h3>'
        )
      end
    end

    def save_as
      @save_as ||= File.basename(@path, '.*') + '.html'
    end

    def h3_regex
      %r{<p>\s*((Статті в іноземних виданнях)|(Тези та матеріали конференцій, статті у нефахових виданнях)|(Тези та матеріали конференцій, статті у нефахових виданнях)|(Основні наукові та навчально-методичні праці)|(Фахові статті)|(Тези конференцій)|(Навчальні посібники)|(Назви наукових праць, монографій)|(Статті у фахових виданнях України)|(Назви наукових праць, монографій, навчальних посібників, підручників)|(Фахові статті за останні 5 років)|(Фахові видання)|(Підручники, навчальні посібники)|(Монографії)|(Список наукових праць)|(Наукові праці)|(Фахові статті та монографії)|(Наукові статті та монографії)|(Тези доповідей на науково-практичних конференціях)|(Наукові статті)|(Основні наукові та навчально-методичні праці)|(Фахові статті)|(Тези конференцій)|(Навчально-методичні праці)|(Науково-дослідна робота)|(Участь у виконанні науково-дослідних тем)|(Навчальні та навчально-методичні посібники під грифом МОН України)|(Керівництво науковою роботою студентів)|(Статті у виданнях, що включені до наукометричних баз, зокрема Scopus або Web of Science Core Collection)|(Навчальні та навчально-методичні посібники під грифом МОН України)|(Статті у наукових фахових виданнях)|(За матеріалами наукових конференцій)|(Інші публікації)|(Монографія)|(Основні публікації за напрямом)|(Участь у конференціях і семінарах)):?\s*</p>}
    end
  end
end
