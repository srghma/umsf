module Umsf
  class Extractor
    def initialize(path)
      @path = path
    end

    def extract_images
      case @path
      when /.docx$/i
        extract_docx
      when /.doc$/i
        extract_doc
      else
        puts path + 'cannot be handled'
      end
    end

    def extract_docx
      require 'shellwords'
      path = Shellwords.escape(@path)

      `unzip #{path} "word/media/*"`
    end

    def extract_doc
      # require 'shellwords'
      # path = Shellwords.escape(@path)

      # `abiword --to=html #{path}`
    end
  end
end
