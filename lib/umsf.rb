require 'umsf/version'
require 'umsf/visitor'
require 'umsf/parser'
require 'umsf/extractor'

module Umsf
  def self.visit(path)
    Visitor.new(path)
  end
end
