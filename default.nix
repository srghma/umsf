{ stdenv, bundlerEnv, ruby, antiword, abiword, git, makeWrapper }:

let
  pname = "umsf";
  version = "0.1";

  gems = bundlerEnv {
    name = "${pname}-${version}";
    inherit ruby;
    gemfile = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset = ./gemset.nix;
  };

in
stdenv.mkDerivation {
  inherit pname version;
  name = "${pname}-${version}";
  src = ./.;
  buildInputs = [gems ruby antiword abiword git makeWrapper];

  installPhase = ''
    mkdir -p $out/bin
    makeWrapper $src/bin/${pname} $out/bin/${pname} \
      --prefix PATH : "${antiword}/bin" \
      --prefix PATH : "${abiword}/bin"
  '';

  passthru = {
    inherit ruby gems;
  };
}
