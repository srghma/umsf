# coding: utf-8

name = 'umsf'

require File.dirname(__FILE__) + "/lib/#{name}/version"

Gem::Specification.new do |spec|
  spec.name          = name
  spec.version       = Umsf::VERSION
  spec.authors       = ['Sergey Homa']
  spec.email         = ['srgma@gmail.com']

  spec.summary       = '_____'
  spec.description   = '_____'
  spec.homepage      = "http://rubygems.org/gems/#{name}"
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'http://mygemserver.com'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir      = 'bin'
  spec.executables = [name]

  spec.add_dependency 'doc_ripper'
end
